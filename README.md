# 高德POI爬取

#### 介绍
针对每个城市，做城市网格切分，然后根据切分的网格爬取网格内高德POI数据。

#### 爬虫说明
1.根据城市获取城市边界。<br>
2.城市网格切分，这里是500*500小网格。<br>
3.并发请求，针对每个小网格获取网格内POI数据，主要POI类型为（餐饮服务，购物服务，住宿服务，商务住宅，等）。<br>
4.导出csv。<br>

### 技术应用
1.request 原生请求。<br>
2.psycopg2 连接数据库保存数据。<br>
3.ThreadPoolExecutor 多线程并发请求<br>
4.csv 导出csv <br>

### 城市网格图
<div style="display:flex; height:auto; justify-content: center;">
<img src="https://gitee.com/xudaile/gd_poi/raw/master/images/成都市网格图.jpg" width="800px"/>
</div>

### 导出csv图
<div style="display:flex; height:auto; justify-content: center;">
<img src="https://gitee.com/xudaile/gd_poi/raw/master/images/成都POI.jpg" width="800px"/>
</div>