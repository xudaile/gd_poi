def list_split_by_size(lens, slice_size):
    if lens < 1:
        return None
    list = []
    if slice_size > lens:
        list.append("0," + str(lens - 1))
        return list
    is_z = lens % slice_size == 0
    count = lens / slice_size if is_z else lens / slice_size + 1
    for i in range(0, int(count)):
        begin = i * slice_size
        end = (i + 1) * slice_size - 1
        if end > lens - 1:
            end = lens - 1
        list.append(str(begin) + "," + str(end))
    return list